import numpy as np
import csv

def readData(fileName, featureSize):
    listData = []
    with open(fileName, "r") as csvfile:
        datareader = csv.reader(csvfile)
        for row in datareader:
            featuresOfInterest = row[:featureSize]
            parsedFeatures = []
            for e in featuresOfInterest:
                parsedFeatures.append(int(e))
            listData.append(parsedFeatures)
    return np.asarray(listData)

readData("SCFD_Attack1.csv",15)
