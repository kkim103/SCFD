from sklearn.cluster import KMeans
import numpy as np

def getCovariance(singleDataGroup):
    meanVector = getMean(singleDataGroup)
    n = len(singleDataGroup)
    featureSize = len(singleDataGroup[0])
    result = np.zeros((featureSize, featureSize))
    for i in range(n):
        diff = meanVector - singleDataGroup[i]
        # print(singleDataGroup[i])
        result += np.dot(np.transpose(diff), diff)
    return result/(n-1)

def getMean(singleDataGroup):
    return np.mean(singleDataGroup, axis=0)

def partitionByLabel(dataset, label):
    labelDict = {}
    output = {}
    for i in range(len(dataset)):
        if label[i] not in labelDict:
            labelDict[label[i]] = []
        labelDict[label[i]].append(dataset[i])
    for k in labelDict:
        output[k] = np.asarray(labelDict[k])
    return output

# extend KMeans to Global KMeans with the same API as the normal KMeans
class globalKMeans:

    def __init__(self, maxK, n_init=10,max_iter=300,tol=0.01,precompute_distances='auto' , verbose=0, random_state=None, copy_x=True, n_jobs=1, algorithm='auto'):
        self._maxK = maxK
        self._n_init=n_init
        self._max_iter=max_iter
        self._tol=tol
        self._precompute_distances=precompute_distances
        self._verbose=verbose
        self._random_state=random_state
        self._copy_x=copy_x
        self._n_jobs=n_jobs
        self._algorithm=algorithm
        self._final_model = None
        self._maxK = maxK
        self._cov = None
        self._partition = None
        self._centroids = None

    def fit(self, X, y=None):
        # TODO: implement globalKMeans

        candidateInitialCenters = []

        for k in range(1, self._maxK+1):
            errorModel = {}
            for x in X:
                candidateInitialCenters.append(x)
                candidateModel = KMeans(n_clusters=k, max_iter=self._max_iter,tol=self._tol,precompute_distances=self._precompute_distances, verbose=self._verbose, random_state=self._random_state, copy_x=self._copy_x, n_jobs=self._n_jobs, algorithm=self._algorithm)
                candidateModel.fit(X, y)
                errorModel[candidateModel.inertia_] = x
                candidateInitialCenters.pop()

            sorted_d = [(k,v) for k,v in errorModel.items()]
            #  TODO: Sort the tuple by the value
            sorted_d.sort(key=lambda tup: tup[0])
            # print(sorted_d[-1])
            candidateInitialCenters.append(sorted_d[-1][1])

        self._final_model = KMeans(n_clusters=len(candidateInitialCenters), n_init=self._n_init,max_iter=self._max_iter,tol=self._tol,precompute_distances=self._precompute_distances, verbose=self._verbose, random_state=self._random_state, copy_x=self._copy_x, n_jobs=self._n_jobs, algorithm=self._algorithm)
        self._final_model.fit(X, y)

        # TASK1: calculate covariance matrix

        # group examples by labels
        self._partition = partitionByLabel(X, self._final_model.labels_)
        
        self._cov = {}
        self._invcov = {}
        self._centroids = {}
        for p,v in self._partition.items():
            self._cov[p] = getCovariance(v)
            self._invcov[p] = np.linalg,inv(self._cov[p])
            self._centroids[p] = getMean(v)


    def fit_predict(self, X, y=None):
        return self._final_model.fit_predict(X, y)

    def fit_transform(self, X, y=None):
        return None

    def get_params(self, deep=True):
        return None

    def predict(self, X):
        #  TODO: implement mahalanobis distance
        result = []

        for i in range(len(X)):
            dataPoint = X[i]

            candidateDistances = {}
            for partition, centroid in self._centroids.items():
                diff = centroid - dataPoint
                candidateDistances[partition] = np.sqrt(
                        np.dot(
                            np.dot(
                                np.transpose(diff), self._invcov[partition]),
                            diff)
                        )

        return result

    def score(self, X):
        return self._final_model.score(X)

    def set_params(self, **params):
        return self._final_model._set_params(**params)

    def transform(self, X):
        return self._final_model.transform(X)

# TODO: Test run the Global-KMeans
if __name__ == "__main__":
    dataset = np.random.rand(10,3)
    # print(getCovariance(dataset))
    gkm = globalKMeans(5)
    gkm.fit(dataset)
    # print(gkm._final_model.labels_)

    test_data = np.random.rand(3,3)
    

    
